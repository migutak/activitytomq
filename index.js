var amqp = require('amqplib/callback_api');
var express = require("express");
var cors = require('cors');
var app = express();

const corsConfig = {
    credentials: true,
    origin: true,
};

app.use(cors(corsConfig));
app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));

app.get('/activitylogtomq/newactivity', function (req, res) {
    res.status(200).json({
        status: 'OK'
    });
});

app.post("/activitylogtomq/newactivity", function (request, response) {
    amqp.connect(process.env.RABBITMQ || 'amqp://localhost', function (error0, connection) {
        if (error0) {
            throw error0; 
        }
        connection.createChannel(function (error1, channel) {
            if (error1) {
                console.log(error1);
                return response.status(500).json({
                    success: false,
                    message: 'Error creating bucket.' + error1.message
                })
            }
            var queue = request.query.queue;
            var msg = request.body

            channel.assertQueue(queue, {
                durable: true
            });
            channel.sendToQueue(queue, Buffer.from(JSON.stringify(msg)), {
                persistent: true
            });
            console.log(" [x] Sent '%s'", msg);
            response.status(200).json({
                success: true,
                message: 'Msg added !'
            })
        });
        setTimeout(function () {
            connection.close();
            //process.exit(0)
        }, 500);
    });
});

var server = app.listen(process.env.PORT || 5400, function () {
    console.log("Listening on port %s...", server.address().port);
});