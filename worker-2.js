var amqp = require('amqplib/callback_api');
var client = require('./connection.js');

/*client.cluster.health({}, function (err, resp, status) {
  console.log("-- Client Health --", resp);
});*/

amqp.connect(process.env.RABBITMQ || 'amqp://localhost', function (error0, connection) {
  if (error0) {
    throw error0;
  }
  connection.createChannel(function (error1, channel) {
    if (error1) {
      throw error1;
    }
    var queue = 'activitylogews';

    channel.assertQueue(queue, {
      durable: true
    });
    channel.prefetch(1);
    console.log(" [*worker-2] Waiting for messages in %s. To exit press CTRL+C", queue);
    channel.consume(queue, function (msg) {
      var secs = msg.content.toString().split('.').length - 1;

      console.log(" [xworker-2] Received %s", msg.content.toString());
      client.index({
        index: 'activitylogews',
        body: msg.content.toString()
      }, function (err, resp, status) {
        console.log(resp);
      });

      setTimeout(function () {
        console.log(" [x] Done");
        channel.ack(msg);
      }, secs * 1000);
    }, {
      // manual acknowledgment mode,
      // see ../confirms.html for details
      noAck: false
    });
  });
});